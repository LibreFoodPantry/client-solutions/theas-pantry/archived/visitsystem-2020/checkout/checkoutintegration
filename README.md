# Introduction
Docker Compose can be used to create MongoDB, Spring Boot, and Angular containers
with a single command. To correctly use this Docker-Compose script, clone this
repository and then clone the CheckoutFrontEnd and CheckoutService repositories
within it, so your hierarchy looks like this:


- CheckoutIntegration
    - CheckoutFrontEnd
        - Dockerfile  
        ...
    - CheckoutService
        - Dockerfile  
        ...
 - docker-compose.yml
 - Dockerfile
    

This hierarchy is necessary for docker-compose to find the Dockerfiles required
to create the Angular and Spring Boot containers. Each of the Dockerfiles can
be used to create Docker Containers in isolation, but the docker-compose.yml
file in the CheckoutIntegration repository can connect them to each other and
MongoDB.

# Build Spring Boot
Before you can build the containers, Spring Boot will need to be built so that
Docker can copy the .jar file to a container.

In a terminal window within the CheckoutService directory, build the project.

On Mac and Linux:
```
/CheckoutService$ ./gradlew clean build
```
Or on Windows:
```
\CheckoutService> gradlew clean build
```

You can also build the project using the IDE of your choice.

# Run Docker Compose

First, [install Docker Compose](https://docs.docker.com/compose/install/)

You can then run Docker Compose and it will create the containers.

In a terminal window within the CheckoutIntegration directory, run the docker-compose.yml file.

```
/CheckoutIntegration$ docker-compose build && docker-compose up
```

# Use Angular App

Enable CORS (cross-origin resource sharing) on the web browser of your choice
and navigate to the url shown in the terminal output from the Angular container
(currently http://localhost:4200/)

# Check MongoDB Database

If you need to check the data in MongoDB, you can enter the Docker container.

```
/CheckoutIntegration$ docker exec -it mongo bash
```

This command will open container and typing the command "mongo" will open
the Mongo shell within the container. The database can then be explored as normal.  

The Mongo Database is mounted from the local CheckoutIntegration/data folder, 
so changes will be persistent if containers are deleted. This folder will be
created if it does not exist when the database is created by the application.

